#-------------------------------------------------
#
# Project created by QtCreator 2014-07-14T23:03:32
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = NOKIA_UDP
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    ../../Downloads/UDP_client_server/untitled/clientudp.cpp \
    ../../Downloads/UDP_client_server/untitled/serverudp.cpp \
    ../../Downloads/UDP_client_server/untitled/main.cpp

OTHER_FILES += \
    ../../Downloads/UDP_client_server/untitled/untitled.pro \
    ../../Downloads/UDP_client_server/untitled/untitled.pro.user

HEADERS += \
    ../../Downloads/UDP_client_server/untitled/clientudp.cpp.autosave \
    ../../Downloads/UDP_client_server/untitled/clientudp.h \
    ../../Downloads/UDP_client_server/untitled/serverudp.h
