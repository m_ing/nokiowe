#ifndef SERVERUDP_H
#define SERVERUDP_H

#include "serverudp.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include <iostream>

class ServerUDP
{
public:
    ServerUDP(int);
    ~ServerUDP();
    void run();
    void setUp();
private:
    int portNumber_, socket_, receivedBytes_;
    struct sockaddr_in hostAddress;
    unsigned char buffer_[2048];
};

#endif // SERVERUDP_H
