#-------------------------------------------------
#
# Project created by QtCreator 2014-07-14T12:16:20
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = untitled
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   +=c++11

TEMPLATE = app


SOURCES += main.cpp \
    serverudp.cpp \
    clientudp.cpp

HEADERS += \
    serverudp.h \
    clientudp.h
