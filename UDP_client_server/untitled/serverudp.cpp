#include "serverudp.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include <iostream>

ServerUDP::ServerUDP(int portNumber) : portNumber_(portNumber)
{
}

ServerUDP::~ServerUDP()
{
    //close(socket_);
    std::cout<<"Closing socket"<<std::endl;
}

void ServerUDP::setUp()
{
    socket_ = socket(AF_INET, SOCK_DGRAM, 0);
    if(socket_<0)
    {
        std::cerr<<"Socket cannot be opened"<<std::endl;
    //    exit(1);
    }

    bzero(&hostAddress, sizeof(hostAddress));
    hostAddress.sin_family = AF_INET;
    hostAddress.sin_port = htons(portNumber_);
    hostAddress.sin_addr.s_addr = INADDR_ANY;
    int bindResult = bind(socket_, (struct sockaddr *) &hostAddress, sizeof(hostAddress));
    if (bindResult < 0)
    {
        std::cerr<<"Binding error"<<std::endl;
    //    exit(1);
    }
}

void ServerUDP::run()
{
    for (;;)
    {
        std::cout<<"Waiting on port: "<<portNumber_<<std::endl;
        receivedBytes_ = recvfrom(socket_, buffer_, 2048, 0, 0, 0);
        std::cout<<"Received "<<receivedBytes_<<" bytes."<<std::endl;
        if (receivedBytes_ > 0)
        {
            buffer_[receivedBytes_] = 0;
            std::cout<<"Received message: "<<buffer_<<std::endl;
        }
    }
}
