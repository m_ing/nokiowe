#ifndef CLIENTUDP_H
#define CLIENTUDP_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

class ClientUDP
{
public:
    ClientUDP(int);
    void run();
    void send();
private:
    int socket_, portNumber_;
    unsigned char buffer_;
    struct sockaddr_in clientAddress_, serverAddress_;
    struct hostent *server_;
};

#endif // CLIENTUDP_H
