#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <strings.h>
#include "serverudp.h"
#include "clientudp.h"

int main(int argc, char *argv[])
{
    ServerUDP nowyServer(3000);
    nowyServer.setUp();
    nowyServer.run();
}
